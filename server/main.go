package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"lelux.net/kardbord"
	"lelux.net/kardbord/all"
)

func Track(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	ssName := vars["service"]
	numbers := strings.Split(vars["number"], ",")

	var shipments []*kardbord.Shipment
	if ss, ok := all.SingleTrackable[ssName]; ok {
		shipments = make([]*kardbord.Shipment, len(numbers))
		for i, number := range numbers {
			s, err := ss.TrackSingle(number)
			if err != nil {
				return
			}
			shipments[i] = s
		}
	} else if ss, ok := all.MultipleTrackable[ssName]; ok {
		shipments2, err := ss.TrackMultiple(numbers...)
		if err != nil {
			return
		}
		shipments = shipments2
	} else {
		return
	}

	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(shipments)
}

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/{service}/{number}", Track).Methods(http.MethodGet)

	log.Fatal(http.ListenAndServe(":8080", r))
}
