package gls

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"lelux.net/kardbord"
)

func TestTracking(t *testing.T) {
	ss := GLS{}

	s, err := ss.TrackSingle("22895090362")
	assert.Nil(t, err)
	assert.NotNil(t, s)

	es := s.Events
	assert.Equal(t, 13, len(es))

	e := es[0]
	assert.Equal(t, kardbord.StatusUnknown, e.Status)
	assert.Equal(t, "The parcel label for the pickup has been produced.", e.Message)
	assert.Equal(t, time.Date(2021, 9, 7, 6, 44, 10, 0, time.UTC), e.Time.UTC())
	assert.Equal(t, "Ireland (IE)", e.Location)

	e = es[12]
	assert.Equal(t, kardbord.StatusDelivered, e.Status)
	assert.Equal(t, "The parcel has been delivered.", e.Message)
	assert.Equal(t, time.Date(2021, 9, 15, 11, 36, 0, 0, time.UTC), e.Time.UTC())
	assert.Equal(t, "Lucernate di Rho (MI), Italy (IT)", e.Location)

	assert.Equal(t, 27.75, s.Weight)
}

func TestTrackingMissing(t *testing.T) {
	ss := GLS{}

	s, err := ss.TrackSingle("x")
	assert.Nil(t, err)
	assert.Nil(t, s)
}
