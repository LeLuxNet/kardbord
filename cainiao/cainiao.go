package cainiao

import (
	"encoding/json"
	"fmt"
	"html"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"lelux.net/kardbord"
)

const prefix = `id="waybill_list_val_box">`
const suffix = "</textarea>"

var _ kardbord.MultipleTrackable = (*Cainiao)(nil)

type Cainiao struct{}

func (ss *Cainiao) TrackMultiple(ids ...string) ([]*kardbord.Shipment, error) {
	res, err := http.Get("https://global.cainiao.com/detail.htm?mailNoList=" + url.QueryEscape(strings.Join(ids, ",")))
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	str := string(b)

	i := strings.Index(str, prefix) + len(prefix)
	str = str[i:]

	i = strings.Index(str, suffix)
	str = html.UnescapeString(str[:i])

	j := &response{}
	err = json.Unmarshal([]byte(str), j)
	if err != nil {
		return nil, err
	}

	s := make([]*kardbord.Shipment, len(ids))
	for i, js := range j.Data {
		if !js.Success {
			continue
		}

		jes := js.Section2.DetailList
		e := make([]kardbord.Event, len(jes))

		var status kardbord.Status

		for i := 0; i < len(e); i++ {
			je := jes[len(e)-1-i]

			if je.Status != "" {
				index := strings.Index(je.Status, "_")
				if index == -1 {
					switch je.Status {
					case "PICKEDUP", "SHIPPING":
						status = kardbord.StatusTransit
					case "SIGNIN":
						status = kardbord.StatusDelivered
					}
				} else {
					switch je.Status[:index] {
					case "DEPART", "ARRIVED":
						status = kardbord.StatusTransit
					case "RETURNED":
						status = kardbord.StatusReturned
					case "RDESTORYED":
						status = kardbord.StatusFailure
					}
				}
			}

			var t time.Time
			t, err = time.Parse("2006-01-02 15:04:05-07", fmt.Sprintf("%s%c0%s", je.Time, je.TimeZone[0], je.TimeZone[1:]))
			if err != nil {
				return nil, err
			}

			e[i] = kardbord.Event{
				Status:  status,
				Message: je.Desc,
				Time:    t,
			}
		}

		s[i] = &kardbord.Shipment{
			Events: e,
		}
	}

	return s, nil
}

type response struct {
	Data []struct {
		Section2 struct {
			DetailList []struct {
				Desc     string
				Status   string
				Time     string
				TimeZone string
			}
		}
		Success bool
	}
}
