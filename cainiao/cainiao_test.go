package cainiao

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"lelux.net/kardbord"
)

func TestTracking(t *testing.T) {
	ss := Cainiao{}

	s, err := ss.TrackMultiple("UV416672464UZ", "xxxxxxxxxxxxxxxxxxxxxx")
	assert.Nil(t, err)
	assert.Equal(t, 2, len(s))

	assert.Equal(t, 9, len(s[0].Events))

	e := s[0].Events[2]
	assert.Equal(t, kardbord.StatusTransit, e.Status)
	assert.Equal(t, "Outbound in sorting center", e.Message)
	assert.Equal(t, time.Date(2021, 6, 27, 16, 56, 46, 0, time.UTC), e.Time.UTC())
	assert.Equal(t, "", e.Location)

	e = s[0].Events[6]
	assert.Equal(t, kardbord.StatusTransit, e.Status)
	assert.Equal(t, "Depart from  transit country or district", e.Message)
	assert.Equal(t, time.Date(2021, 7, 5, 5, 22, 37, 0, time.UTC), e.Time.UTC())
	assert.Equal(t, "", e.Location)

	assert.Nil(t, s[1])
}
