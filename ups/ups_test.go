package ups

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"lelux.net/kardbord"
)

func TestTracking(t *testing.T) {
	ss := UPS{}

	s, err := ss.TrackSingle("801947822105327247")
	assert.Nil(t, err)
	assert.NotNil(t, s)

	es := s.Events
	// assert.Equal(t, 13, len(es))

	e := es[0]
	assert.Equal(t, kardbord.StatusProcessing, e.Status)
	assert.Equal(t, "Order information received", e.Message)
	assert.Equal(t, time.Date(2021, 8, 9, 15, 57, 0, 0, time.UTC), e.Time)
	assert.Equal(t, "United States", e.Location)

	e = es[5]
	assert.Equal(t, kardbord.StatusTransit, e.Status)
	assert.Equal(t, "Shipment Acceptance at international carrier", e.Message)
	assert.Equal(t, time.Date(2021, 8, 12, 10, 24, 0, 0, time.UTC), e.Time)
	assert.Equal(t, "United States", e.Location)

	e = es[12]
	assert.Equal(t, kardbord.StatusTransit, e.Status)
	assert.Equal(t, "Package arrived at international carrier", e.Message)
	assert.Equal(t, time.Date(2021, 8, 22, 0, 23, 0, 0, time.UTC), e.Time)
	assert.Equal(t, "Spain", e.Location)

	assert.Equal(t, 1.088621688, s.Weight)
}

func TestTrackingMissing(t *testing.T) {
	ss := UPS{}

	s, err := ss.TrackSingle("x")
	assert.Nil(t, err)
	assert.Nil(t, s)
}
