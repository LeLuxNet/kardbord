package ups

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"

	"lelux.net/kardbord"
)

var _ kardbord.SingleTrackable = (*UPS)(nil)

type UPS struct {
	xsrfToken string
	csrfToken string
	tokenLock sync.Mutex
}

var xsrfTokenName = "X-XSRF-TOKEN-ST"
var csrfTokenName = "X-CSRF-TOKEN"

func (ss *UPS) getToken() (string, string, error) {
	ss.tokenLock.Lock()
	defer ss.tokenLock.Unlock()

	if ss.xsrfToken != "" && ss.csrfToken != "" {
		return ss.xsrfToken, ss.csrfToken, nil
	}

	res, err := http.Get("https://www.ups.com/track")
	if err != nil {
		return "", "", err
	}
	err = res.Body.Close()
	if err != nil {
		return "", "", err
	}

	for _, c := range res.Cookies() {
		switch c.Name {
		case xsrfTokenName:
			ss.xsrfToken = c.Value
		case csrfTokenName:
			ss.csrfToken = c.Value
		}
	}

	if ss.xsrfToken == "" || ss.csrfToken == "" {
		return "", "", http.ErrNoCookie
	}

	return ss.xsrfToken, ss.csrfToken, nil
}

func (ss *UPS) TrackSingle(id string) (*kardbord.Shipment, error) {
	xsrf, csrf, err := ss.getToken()
	if err != nil {
		return nil, err
	}

	reqB, err := json.Marshal(&request{
		TrackingNumber: []string{id},
	})
	if err != nil {
		return nil, err
	}

	locale := kardbord.Lang[:strings.IndexByte(kardbord.Lang, '_')]

	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("https://www.ups.com/track/api/Track/GetStatus?loc=%s_US", url.QueryEscape(locale)), bytes.NewReader(reqB))
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")

	req.Header.Add("X-XSRF-TOKEN", xsrf)
	req.AddCookie(&http.Cookie{
		Name:  "X-CSRF-TOKEN",
		Value: csrf,
	})

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	j := &response{}
	err = json.Unmarshal(b, j)
	if err != nil {
		return nil, err
	}

	if j.StatusCode == "402" ||
		(*j.Details)[0].ErrorCode == "504" {
		return nil, nil
	}

	js := (*j.Details)[0]
	jes := js.Activities

	e := make([]kardbord.Event, len(jes))
	for i, je := range jes {
		var status kardbord.Status
		switch je.Code {
		case "MP", "VS":
			status = kardbord.StatusProcessing
		case
			"Q1",
			"Q2",
			"Q3",
			"Q4",
			"ZS",
			"ZT",
			"ZU",
			"ZV",
			"ZR",
			"ZY",
			"YH",
			"1A":
			status = kardbord.StatusTransit
		case "YC":
			status = kardbord.StatusDelivered
		}

		t, err := time.Parse("01/02/20063:04 PM", je.Date+je.Time[:len(je.Time)-3]+string(je.Time[len(je.Time)-2]))
		if err != nil {
			return nil, err
		}

		e[len(e)-1-i] = kardbord.Event{
			Status:   status,
			Message:  je.ActivityScan,
			Time:     t,
			Location: je.Location,
		}
	}

	var weight float64
	if js.AdditionalInformation.WeightUnit == "LBS" {
		weight, _ = strconv.ParseFloat(js.AdditionalInformation.Weight, 64)
	}

	return &kardbord.Shipment{
		Events: e,

		Weight: weight * 0.45359237,
	}, nil
}

type request struct {
	TrackingNumber []string
}

type response struct {
	StatusCode string
	Details    *[]struct {
		ErrorCode             string
		AdditionalInformation struct {
			Weight     string
			WeightUnit string
		}
		Activities []struct {
			Date         string
			Time         string
			Location     string
			ActivityScan string
			Code         string `json:"actCode"`
		} `json:"shipmentProgressActivities"`
	} `json:"trackDetails"`
}
