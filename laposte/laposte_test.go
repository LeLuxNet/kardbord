package laposte

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"lelux.net/kardbord"
)

func TestTracking(t *testing.T) {
	ss := LaPoste{}

	s, err := ss.TrackSingle("CC994166978FR")
	assert.Nil(t, err)
	assert.NotNil(t, s)

	es := s.Events
	assert.Equal(t, 7, len(es))

	e := es[0]
	assert.Equal(t, kardbord.StatusTransit, e.Status)
	assert.Equal(t, "Your parcel has been delivered to a postal point.", e.Message)
	assert.Equal(t, time.Date(2021, 8, 19, 8, 7, 0, 0, time.UTC), e.Time.UTC())
	assert.Equal(t, "", e.Location)

	e = es[3]
	assert.Equal(t, kardbord.StatusFailure, e.Status)
	assert.Equal(t, "The delivery address is incomplete and we cannot deliver your parcel. We are looking for additional information to ensure the safe delivery of your parcel. Please contact our customer service to provide the necessary additional information.", e.Message)
	assert.Equal(t, time.Date(2021, 8, 23, 13, 57, 0, 0, time.UTC), e.Time.UTC())
	assert.Equal(t, "", e.Location)
}

func TestTrackingMissing(t *testing.T) {
	ss := LaPoste{}

	s, err := ss.TrackSingle("x")
	assert.Nil(t, err)
	assert.Nil(t, s)
}
