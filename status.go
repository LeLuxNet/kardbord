package kardbord

import "fmt"

type Status int

const (
	StatusUnknown Status = iota
	StatusProcessing
	StatusTransit
	StatusDelivered
	StatusReturned
	StatusFailure
)

func (s Status) MarshalJSON() ([]byte, error) {
	var str string
	switch s {
	case StatusProcessing:
		str = "processing"
	case StatusTransit:
		str = "transit"
	case StatusDelivered:
		str = "delivered"
	case StatusReturned:
		str = "returned"
	case StatusFailure:
		str = "failure"
	default:
		str = "unknown"
	}

	return []byte(fmt.Sprintf(`"%s"`, str)), nil
}
