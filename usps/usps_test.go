package usps

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"lelux.net/kardbord"
)

func TestTracking(t *testing.T) {
	userId := os.Getenv("USPS_USERID")

	if userId == "" {
		t.SkipNow()
	}

	ss := USPS{UserID: userId}

	s, err := ss.TrackMultiple("9400111108435461875745", "x")
	assert.Nil(t, err)
	assert.Equal(t, 2, len(s))

	assert.Equal(t, 10, len(s[0].Events))

	e := s[0].Events[0]
	assert.Equal(t, kardbord.StatusProcessing, e.Status)
	assert.Equal(t, "Pre-Shipment Info Sent to USPS, USPS Awaiting Item", e.Message)
	assert.Equal(t, time.Date(2021, 8, 23, 0, 0, 0, 0, time.UTC), e.Time.UTC())
	assert.Equal(t, "", e.Location)

	e = s[0].Events[5]
	assert.Equal(t, kardbord.StatusTransit, e.Status)
	assert.Equal(t, "Arrived at USPS Facility", e.Message)
	assert.Equal(t, time.Date(2021, 8, 24, 19, 22, 0, 0, time.UTC), e.Time.UTC())
	assert.Equal(t, "INDIANAPOLIS, IN 46242", e.Location)

	assert.Nil(t, s[1])
}
