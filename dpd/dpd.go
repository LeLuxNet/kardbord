package dpd

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	"lelux.net/kardbord"
)

var client = &http.Client{
	CheckRedirect: func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	},
}

var _ kardbord.SingleTrackable = (*DPD)(nil)

type DPD struct{}

func (ss *DPD) TrackSingle(id string) (*kardbord.Shipment, error) {
	res, err := client.Get(fmt.Sprintf("https://tracking.dpd.de/rest/plc/%s/%s", url.PathEscape(kardbord.Lang), url.PathEscape(id)))
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode == http.StatusFound {
		return nil, nil
	}

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	j := &response{}
	err = json.Unmarshal(b, j)
	if err != nil {
		return nil, err
	}

	jes := j.Response.Data.Scan.Scan

	e := make([]kardbord.Event, len(jes))
	for i, je := range jes {
		var status kardbord.Status
		switch je.Description.Label {
		case "1", "2", "3", "5":
			status = kardbord.StatusTransit
		case "13":
			status = kardbord.StatusDelivered
		case "18":
			status = kardbord.StatusProcessing
		}

		t, err := time.Parse("2006-01-02T15:04:05", je.Date)
		if err != nil {
			return nil, err
		}

		e[i] = kardbord.Event{
			Status:   status,
			Message:  je.Description.Content[0],
			Time:     t,
			Location: je.Data.Location,
		}
	}

	return &kardbord.Shipment{
		Events: e,
	}, nil
}

type response struct {
	Response struct {
		Data struct {
			Scan struct {
				Scan []struct {
					Date string
					Data struct {
						Location string
					} `json:"scanData"`
					Description struct {
						Label   string
						Content []string
					} `json:"scanDescription"`
				} `json:"scan"`
			} `json:"scanInfo"`
		} `json:"parcelLifeCycleData"`
	} `json:"parcellifecycleResponse"`
}
