package hermes

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"lelux.net/kardbord"
)

func TestTracking(t *testing.T) {
	ss := Hermes{}

	s, err := ss.TrackSingle("02233169002839")
	assert.Nil(t, err)
	assert.NotNil(t, s)

	es := s.Events
	assert.Equal(t, 9, len(es))

	e := es[0]
	assert.Equal(t, kardbord.StatusUnknown, e.Status)
	assert.Equal(t, "Die Sendung wurde Hermes elektronisch angekündigt.", e.Message)
	assert.Equal(t, time.Date(2021, 8, 21, 18, 38, 0, 0, time.UTC), e.Time)
	assert.Equal(t, "", e.Location)

	e = es[8]
	assert.Equal(t, kardbord.StatusDelivered, e.Status)
	assert.Equal(t, "Die Sendung wurde zugestellt.", e.Message)
	assert.Equal(t, time.Date(2021, 8, 26, 11, 27, 0, 0, time.UTC), e.Time)
	assert.Equal(t, "", e.Location)
}

func TestTrackingMissing(t *testing.T) {
	ss := Hermes{}

	s, err := ss.TrackSingle("x")
	assert.Nil(t, err)
	assert.Nil(t, s)
}
