package postnord

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"lelux.net/kardbord"
)

var _ kardbord.SingleTrackable = (*PostNord)(nil)

type PostNord struct{}

func (ss *PostNord) TrackSingle(id string) (*kardbord.Shipment, error) {
	req, err := http.NewRequest(http.MethodGet, "https://api2.postnord.com/rest/shipment/v5/trackandtrace/ntt/shipment/recipientview", nil)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	q.Set("id", id)

	locale := kardbord.Lang[:strings.IndexByte(kardbord.Lang, '_')]
	switch locale {
	case "sv", "no", "da", "fi":
		q.Set("locale", locale)
	}

	req.URL.RawQuery = q.Encode()

	req.Header.Add("x-bap-key", "web-ncp")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	j := &response{}
	err = json.Unmarshal(b, j)
	if err != nil {
		return nil, err
	}

	if len(j.Response.Shipments) == 0 {
		return nil, nil
	}
	js := j.Response.Shipments[0].Items[0]
	jes := js.Events

	e := make([]kardbord.Event, len(jes))
	for i, je := range jes {
		var status kardbord.Status
		switch je.Status {
		case
			"INFORMED":
			status = kardbord.StatusProcessing
		case
			"EN_ROUTE",
			"AVAILABLE_FOR_DELIVERY",
			"EXPECTED_DELAY":
			status = kardbord.StatusTransit
		case
			"DELIVERED":
			status = kardbord.StatusDelivered
		case
			"RETURNED":
			status = kardbord.StatusReturned
		case
			"STOPPED",
			"DELIVERY_IMPOSSIBLE":
			status = kardbord.StatusFailure
		}

		t, err := time.Parse("2006-01-02T15:04:05", je.Time)
		if err != nil {
			return nil, err
		}

		var location string
		if je.Location.City == "" {
			location = je.Location.DisplayName
		} else {
			location = fmt.Sprintf("%s %s, %s", je.Location.City, je.Location.Postcode, je.Location.Country)
		}

		e[i] = kardbord.Event{
			Status:   status,
			Message:  je.Description,
			Time:     t,
			Location: location,
		}
	}

	s := &kardbord.Shipment{
		Events: e,
	}

	if js.StatedMeasurement.Length.Unit == "m" {
		s.Length, _ = strconv.ParseFloat(js.StatedMeasurement.Length.Value, 64)
	}
	if js.StatedMeasurement.Width.Unit == "m" {
		s.Width, _ = strconv.ParseFloat(js.StatedMeasurement.Width.Value, 64)
	}
	if js.StatedMeasurement.Height.Unit == "m" {
		s.Height, _ = strconv.ParseFloat(js.StatedMeasurement.Height.Value, 64)
	}

	if js.StatedMeasurement.Weight.Unit == "kg" {
		s.Weight, _ = strconv.ParseFloat(js.StatedMeasurement.Weight.Value, 64)
	}

	return s, nil
}

type response struct {
	Response struct {
		Shipments []struct {
			Items []struct {
				StatedMeasurement struct {
					Weight measurement
					Length measurement
					Height measurement
					Width  measurement
					Volume measurement
				}
				Events []struct {
					Time        string `json:"eventTime"`
					Status      string
					Description string `json:"eventDescription"`
					Location    struct {
						DisplayName string
						Country     string
						Postcode    string
						City        string
					}
				}
			}
		}
	} `json:"TrackingInformationResponse"`
}

type measurement struct {
	Value string
	Unit  string
}
